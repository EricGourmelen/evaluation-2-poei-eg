package com.zenika.academy.videogames.service.rawg;

import com.zenika.academy.videogames.domain.VideoGame;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
/**
 * Cette classe set à faire des requêtes HTTP vers l'api de rawg.io (https://rawg.io/apidocs et https://api.rawg.io/docs)
 */
public class RawgDatabaseClient {

    private final String apiKey;
    private final RestTemplate restTemplate;

    @Autowired
    public RawgDatabaseClient(@Value("${videogames.apiKey}") String apiKey) {
        this.restTemplate = new RestTemplate();
        this.apiKey = apiKey;
    }

    public VideoGame getVideoGameFromName(String name) {
        RawgSearchResponse body = restTemplate
                .getForObject("https://api.rawg.io/api/games?key="+apiKey+"&search=" + name, RawgSearchResponse.class);
        if (body != null && body.results.size() > 0) {
            return body.results.get(0);
        }
        else {
            return null;
        }
    }

    private static class RawgSearchResponse {
        private List<VideoGame> results;

        public List<VideoGame> getResults() {
            return results;
        }

        public void setResults(List<VideoGame> results) {
            this.results = results;
        }
    }
}
