package com.zenika.academy.videogames.service;

import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class VideoGamesService {

    private VideoGamesRepository videoGamesRepository;
    private RawgDatabaseClient client;

    @Autowired
    public VideoGamesService(VideoGamesRepository videoGamesRepository, RawgDatabaseClient client) {
        this.videoGamesRepository = videoGamesRepository;
        this.client = client;
    }

    public List<VideoGame> ownedVideoGames(String genre) {
        return this.videoGamesRepository.getAll(genre);
    }

    public Optional<VideoGame> getOneVideoGame(Long id) {
        return this.videoGamesRepository.get(id);
    }

    public VideoGame addVideoGame(String name) throws UnsupportedOperationException {
        VideoGame newGame = client.getVideoGameFromName(name);

        videoGamesRepository.save(newGame);

        return newGame;
    }

    public boolean deleteVideoGame(Long id){
        return this.videoGamesRepository.deleteVideoGame(id);
    }

    public boolean updateStateVideoGame(Long id, boolean finished){
        return this.videoGamesRepository.updateStateVideoGame(id,finished);
    }
}
