package com.zenika.academy.videogames.controllers;

import com.zenika.academy.videogames.controllers.representation.GameFinishedDto;
import com.zenika.academy.videogames.controllers.representation.VideoGameNameRepresentation;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.service.VideoGamesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/video-games")
public class VideoGamesController {

    private VideoGamesService videoGamesService;

    @Autowired
    public VideoGamesController(VideoGamesService videoGamesService) {
        this.videoGamesService = videoGamesService;
    }

    /**
     * Récupérer la liste des jeux vidéos possédés pas l'utilisateur
     *
     * Exemple :
     *
     * GET /video-games
     */
    @GetMapping
    public List<VideoGame> listOwnedVideoGames(@RequestParam String genre) {
        return videoGamesService.ownedVideoGames(genre);
    }

    /**
     * Récupérer un jeu vidéo par son ID
     *
     * Exemple :
     *
     * GET /video-games/3561
     */
    @GetMapping("/{id}")
    public ResponseEntity<VideoGame> getOneVideoGame(@PathVariable("id") Long id) {
        return this.videoGamesService.getOneVideoGame(id).map(v -> ResponseEntity.ok(v)).orElse(ResponseEntity.notFound().build());
    }

    /**
     * Ajouter un jeu vidéo à la collection de l'utilisateur
     *
     * Exemple :
     *
     * POST /video-games
     * Content-Type: application/json
     *
     * {
     *     "name": "The Binding of Isaac: Rebirth"
     * }
     */
    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity<VideoGame> addVideoGameByName(@RequestBody VideoGameNameRepresentation videoGameName) {
        try{
            return new ResponseEntity<VideoGame>(this.videoGamesService.addVideoGame(videoGameName.getName()),HttpStatus.CREATED);
        }catch (UnsupportedOperationException ex){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Supprimer un jeu avec son id
     *
     * Exemple :
     *
     * DELETE /video-games/1234
     */
    @DeleteMapping("/{id}")
    public ResponseEntity deleteVideoGameById(@PathVariable("id") Long id) {
        return new ResponseEntity<> (videoGamesService.deleteVideoGame(id), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity updateStateVideoGame(@PathVariable("id") Long id, @RequestBody GameFinishedDto gameFinishedDto) {
        return new ResponseEntity<> (videoGamesService.updateStateVideoGame(id,gameFinishedDto.isFinished()), HttpStatus.OK);
    }
}
