package com.zenika.academy.videogames.controllers.representation;

public class GameFinishedDto {
    private boolean finished;

    public boolean isFinished() {
        return finished;
    }
}
