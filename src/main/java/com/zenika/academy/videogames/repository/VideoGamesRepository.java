package com.zenika.academy.videogames.repository;

import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Component
public class VideoGamesRepository {

    private HashMap<Long, VideoGame> videoGamesById = new HashMap<>();

    public List<VideoGame> getAll(String genre) {
        List<VideoGame> resultList = new ArrayList<>();
        videoGamesById.forEach((k,v) -> {
            List<Genre> genreList = v.getGenres();
            for (Genre g : genreList) {
                if (g.getName().equals(genre))
                    resultList.add(v);
            }
        });
        return resultList;
    }

    public Optional<VideoGame> get(Long id) {
        return Optional.ofNullable(this.videoGamesById.get(id));
    }

    public void save(VideoGame v) throws UnsupportedOperationException {
        if (!this.videoGamesById.containsKey(v.getId())){
            this.videoGamesById.put(v.getId(), v);
        } else {
            throw new UnsupportedOperationException("La base de donnée contient déjà ce jeu");
        }
    }

    public boolean deleteVideoGame(Long id){
        boolean result = false;
        if (this.videoGamesById.get(id) != null){
            this.videoGamesById.remove(id);
            result = true;
        }
        return result;
    }

    public boolean updateStateVideoGame(Long id, boolean finished){
        boolean result = false;
        if (this.videoGamesById.get(id) != null){
            this.videoGamesById.get(id).setFinished(finished);
            result = true;
        }
        return result;
    }
}
