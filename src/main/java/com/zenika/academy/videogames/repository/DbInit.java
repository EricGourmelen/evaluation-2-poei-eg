package com.zenika.academy.videogames.repository;

import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.service.VideoGamesService;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class DbInit {

    @Autowired
    private VideoGamesRepository videoGamesRepository;
    @Autowired
    private RawgDatabaseClient client;

    @PostConstruct
    private void postConstruct() {
        VideoGame stray = client.getVideoGameFromName("Stray");
        VideoGame eldenRing = client.getVideoGameFromName("Elden Ring");
        VideoGame starfield = client.getVideoGameFromName("Starfield");
        videoGamesRepository.save(stray);
        videoGamesRepository.save(eldenRing);
        videoGamesRepository.save(starfield);
    }
}
